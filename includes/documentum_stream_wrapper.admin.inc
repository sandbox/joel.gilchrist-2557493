<?php

/**
 * @file
 * Generate configuration form and save settings.
 */

/**
 * Form builder for the configuration form.
 */
function documentum_stream_wrapper_configuration_form($form, &$form_state) {
  $repositories = documentum_stream_wrapper_repositories();
  ksort($repositories);

  $header = array();
  $header[] = array('data' => t('Repository Nick Name'));
  $header[] = array('data' => t('Drupal Repository Identifier'));
  $header[] = array('data' => t('REST URL'));
  $header[] = array('data' => t('Repository Name'));
  $header[] = array('data' => t('User Name'));
  $header[] = array('data' => t('Operations'));

  $rows = array();
  $destination = drupal_get_destination();
  foreach ($repositories as $repository_machine_name => $repository) {
    $row = array();

    $plain_repository_machine_name = check_plain($repository_machine_name);

    $row['data']['documentum_repository_nick_name'] = check_plain($repository['documentum_repository_nick_name']);
    $row['data']['documentum_repository_machine_name'] = $plain_repository_machine_name;
    $row['data']['documentum_rest_url'] = check_plain($repository['documentum_rest_url']);
    $row['data']['documentum_repository_name'] = check_plain($repository['documentum_repository_name']);
    $row['data']['documentum_user_name'] = check_plain($repository['documentum_user_name']);

    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => "admin/config/media/documentum/edit/$plain_repository_machine_name",
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => "admin/config/media/documentum/delete/$plain_repository_machine_name",
      'query' => $destination,
    );
    $row['data']['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );

    $rows[] = $row;
  }

  $form['documentum_repositories_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Repositories configured. <a href="@link">Add Repository</a>.',
      array('@link' => url('admin/config/media/documentum/add'))),
  );

  $form['documentum_stream_wrapper_use_current_version'] = array(
    '#title' => t('Always use CURRENT version'),
    '#type' => 'checkbox',
    '#description' => t('Update file references to point to a new CURRENT version
      if/when one becomes available in the repository.  Disable this to always
      use the specific version that was added through add file.'),
    '#required' => FALSE,
    '#default_value' => documentum_stream_wrapper_use_current_version(),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 10,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for configuration form.
 */
function documentum_stream_wrapper_configuration_form_submit($form, &$form_state) {
  variable_set('documentum_stream_wrapper_use_current_version',
    $form_state['values']['documentum_stream_wrapper_use_current_version']);
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Form for adding or editing a Documentum repository.
 */
function documentum_stream_wrapper_repository_add_form($form, &$form_state, $repository_machine_name = NULL) {

  if (isset($repository_machine_name)) {
    $repositories = documentum_stream_wrapper_repositories();
    $repository = $repositories[$repository_machine_name];

    $form['documentum_repository_nick_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Documentum Repository Nick Name'),
      '#description' => t('The Documentum repository user-friendly nick name.'),
      '#default_value' => $repository['documentum_repository_nick_name'],
      '#required' => TRUE,
      '#attributes' => array('disabled' => 'disabled'),
    );

    $form['documentum_repository_machine_name'] = array(
      '#type' => 'textfield',
      '#title' => t("Machine Name"),
      '#required' => TRUE,
      '#description' => t("machine-friendly name."),
      '#default_value' => $repository_machine_name,
      '#attributes' => array('disabled' => 'disabled'),
    );

  }
  else {
    $repository_machine_name = '';
    $repository['documentum_repository_nick_name'] = '';
    $repository['documentum_rest_url'] = '';
    $repository['documentum_repository_name'] = '';
    $repository['documentum_user_name'] = '';
    $repository['documentum_password'] = '';

    $form['documentum_repository_nick_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Documentum Repository Nick Name'),
      '#description' => t('The Documentum repository user-friendly nick name.'),
      '#default_value' => $repository['documentum_repository_nick_name'],
      '#required' => TRUE,
    );

    $form['documentum_repository_machine_name'] = array(
      '#type' => 'machine_name',
      '#title' => t("Machine Name"),
      '#required' => TRUE,
      '#description' => t("machine-friendly name."),
      '#default_value' => $repository_machine_name,
      '#machine_name' => array(
        'exists' => 'documentum_stream_wrapper_machine_name_exists',
        'source' => array('documentum_repository_nick_name'),
      ),
    );

  }

  $form['documentum_rest_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Documentum REST URL'),
    '#description' => t("Base URL for the Documentum Rest Services. Usually of the form http://host:port/context.  Do not include the 'repositories' keyword."),
    '#default_value' => $repository['documentum_rest_url'],
    '#required' => TRUE,
  );

  $form['documentum_repository_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Documentum Repository Name'),
    '#description' => t('The Documentum repository name.'),
    '#default_value' => $repository['documentum_repository_name'],
    '#required' => TRUE,
  );

  $form['documentum_user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Documentum User Name'),
    '#description' => t('The Documentum user name used to access the file.'),
    '#default_value' => $repository['documentum_user_name'],
    '#required' => TRUE,
  );

  $form['documentum_password'] = array(
    '#type' => 'password',
    '#title' => t('Documentum password associated with the user'),
    '#description' => t('The Documentum password used to access the file.  Enter text to change the password.  Leave blank to continue with existing password.'),
    '#default_value' => $repository['documentum_password'],
    '#required' => TRUE,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  return $form;
}

/**
 * Check for the existence of a repository.
 *
 * @param string $name
 *   The machine name of the repository.
 *
 * @return bool
 *   Whether or not the repository already exists or not.
 */
function documentum_stream_wrapper_machine_name_exists($name) {
  $repositories = documentum_stream_wrapper_repositories();
  return isset($repositories[$name]);
}

/**
 * Form validation for adding or editing a repository.
 */
function documentum_stream_wrapper_repository_add_form_validate($form, &$form_state) {
  $repositories = documentum_stream_wrapper_repositories();
  $nick_name = $form_state['values']['documentum_repository_nick_name'];
  $machine_name = $form_state['values']['documentum_repository_machine_name'];

  // Remove current entry.
  unset($repositories[$machine_name]);

  // Check that nick name doesn't already exist.
  $found = FALSE;
  foreach ($repositories as $repository) {
    if ($repository['documentum_repository_nick_name'] === $nick_name) {
      $found = TRUE;
    }
  }

  if ($found) {
    form_set_error('documentum_repository_nick_name', t('Nick name must be unique.'));
    return;
  }
}

/**
 * Form submit handler for adding or editing a repository.
 */
function documentum_stream_wrapper_repository_add_form_submit($form, &$form_state) {
  $repositories = documentum_stream_wrapper_repositories();

  $repository_machine_name = $form_state['values']['documentum_repository_machine_name'];

  $repositories[$repository_machine_name] = array(
    'documentum_repository_nick_name' => $form_state['values']['documentum_repository_nick_name'],
    'documentum_rest_url' => $form_state['values']['documentum_rest_url'],
    'documentum_repository_name' => $form_state['values']['documentum_repository_name'],
    'documentum_user_name' => $form_state['values']['documentum_user_name'],
    'documentum_password' => encrypt($form_state['values']['documentum_password']),
  );

  variable_set('documentum_stream_wrapper_repositories', $repositories);

  drupal_set_message(t('The configuration options have been saved.'));
  $form_state['redirect'] = 'admin/config/media/documentum';
}

/**
 * Form constructor for the repository deletion form.
 */
function documentum_stream_wrapper_repository_delete_confirm($form, &$form_state, $repository_machine_name) {
  if (user_access('administer documentum stream wrapper')) {
    $form['documentum_repository_machine_name'] = array(
      '#type' => 'value',
      '#value' => $repository_machine_name,
    );

    return confirm_form(
      $form,
      t('Are you sure you want to delete the %repository repository?',
        array('%repository' => $repository_machine_name)),
      'admin/config/media/documentum',
      t('This action cannot be undone.  Any files added from this repository will no longer be accessible once this repository has been removed.')
    );
  }
  return array();
}

/**
 * Form submission handler for repository delete confirmation form.
 */
function documentum_stream_wrapper_repository_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $repositories = documentum_stream_wrapper_repositories();
    unset($repositories[$form_state['values']['documentum_repository_machine_name']]);
    variable_set('documentum_stream_wrapper_repositories', $repositories);
    drupal_set_message(t('The repository has been deleted.'));

    $form_state['redirect'] = 'admin/config/media/documentum';
  }
}
