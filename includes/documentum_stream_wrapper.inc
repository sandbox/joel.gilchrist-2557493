<?php

/**
 * @file
 * Extends the RemoteStreamWrapper class.
 *
 * A stream wrapper to handle read-only Documentum content.
 */

define('DOCUMENTUM_STREAM_WRAPPER_BUFFER_SIZE', 8192);
define('DOCUMENTUM_STREAM_WRAPPER_READ_TIMEOUT', 30.0);

/**
 * Documentum Stream Wrapper class.
 */
class DocumentumStreamWrapper extends DrupalRemoteStreamWrapper {

  protected $streamFp = FALSE;
  protected $streamContent = NULL;

  /**
   * Returns a web accessible URL for the resource.
   *
   * This function should return a URL that can be embedded in a web page
   * and accessed from a browser. For this wrapper we use what looks like
   * a local file url combined with a hook_menu() implementation in
   * documentum_stream_wrapper.module to serve up the file from a remote
   * location but to the browser it looks as if it is coming from the
   * drupal backend.
   *
   * @return string
   *   Returns a string containing a web accessible URL for the resource.
   */
  public function getExternalUrl() {
    $prefix = DOCUMENTUM_STREAM_WRAPPER_SCHEME;
    list($scheme, $target) = explode('://', $this->uri, 2);
    if ($prefix == $scheme) {
      $path = str_replace('\\', '/', $target);
      return $GLOBALS['base_url'] . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
    }
  }

  /**
   * Implements abstract public function getDirectoryPath().
   */
  public function getDirectoryPath() {
    return DOCUMENTUM_STREAM_WRAPPER_PROXY_PATH;
  }

  /**
   * Helper method to build the authorization header.
   *
   * @return string
   *   The authorization header value.
   */
  private function getAuthorizationHeader() {
    $path = explode('/', $this->getUri());
    $repository_machine_name = $path[3];

    $repositories = documentum_stream_wrapper_repositories();
    $repository = $repositories[$repository_machine_name];

    return 'Basic ' . base64_encode($repository['documentum_user_name'] . ':' .
      decrypt($repository['documentum_password']));
  }

  /**
   * Returns a REST url for a document's content.
   *
   * @return string
   *   Returns a string to access the REST content for a documentum object.
   */
  private function getContentUrl() {
    $path = explode('/', $this->getUri());
    $repository_machine_name = $path[3];
    $object_id = $path[5];

    $repositories = documentum_stream_wrapper_repositories();
    $repository = $repositories[$repository_machine_name];

    $content_url = $repository['documentum_rest_url'] . '/repositories/'
      . $repository['documentum_repository_name'] . '/objects/'
      . $object_id . '/content-media';
    return $content_url;
  }

  /**
   * Returns a REST url for a document's properties.
   *
   * @return string
   *   Returns a string to access the REST properties for a documentum object.
   */
  private function getPropertiesUrl() {
    $path = explode('/', $this->getUri());
    $repository_machine_name = $path[3];
    $object_id = $path[5];

    $repositories = documentum_stream_wrapper_repositories();
    $repository = $repositories[$repository_machine_name];

    $properties_url = $repository['documentum_rest_url'] . '/repositories/' .
      $repository['documentum_repository_name'] . '/objects/' .
      $object_id;
    return $properties_url;
  }

  /**
   * Open a stream based on the uri.
   *
   * Unlike remote stream wrapper this really
   * does open a stream and only reads one 'chunk' to determine the header
   * information. Much of this method is stolen from drupal_http_request.
   *
   * @param string $uri
   *   The uri to be opened.
   * @param string $mode
   *   The mode must be read-only (r or rb).
   * @param array $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param string $opened_path
   *   Updated to the path that was opened (not used).
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function stream_open($uri, $mode, $options, &$opened_path) {
    $this->uri = $uri;

    $allowed_modes = array('r', 'rb');
    if (!in_array($mode, $allowed_modes)) {
      return FALSE;
    }

    // Open the file descriptor.
    $this->streamContent = NULL;

    $headers['Authorization'] = $this->getAuthorizationHeader();
    $headers['Accept'] = '*/*';

    $options = array(
      'headers' => $headers,
    );

    $url = $this->getContentUrl();

    $result = new stdClass();

    // Parse the URL and make sure we can handle the schema.
    $uri = @parse_url($url);

    if ($uri == FALSE) {
      watchdog(WATCHDOG_ERROR, 'unable to parse URL');
      return FALSE;
    }

    if (!isset($uri['scheme'])) {
      watchdog(WATCHDOG_ERROR, 'missing schema');
      return FALSE;
    }

    timer_start(__FUNCTION__);

    // Set up the default options.
    $options += array(
      'headers' => array(),
      'method' => 'GET',
      'data' => NULL,
      'max_redirects' => 3,
      'timeout' => DOCUMENTUM_STREAM_WRAPPER_READ_TIMEOUT,
      'context' => NULL,
    );

    // Merge the default headers.
    $options['headers'] += array(
      'User-Agent' => 'Drupal (+http://drupal.org/)',
    );

    // stream_socket_client() requires timeout to be a float.
    $options['timeout'] = (float) $options['timeout'];

    switch ($uri['scheme']) {
      case 'http':
        $port = isset($uri['port']) ? $uri['port'] : 80;
        $socket = 'tcp://' . $uri['host'] . ':' . $port;
        // RFC 2616: "non-standard ports MUST, default ports MAY be included".
        // We don't add the standard port to prevent from breaking rewrite rules
        // checking the host that do not take into account the port number.
        $options['headers']['Host']
          = $uri['host'] . ($port != 80 ? ':' . $port : '');
        break;

      case 'https':
        // Note: Only works when PHP is compiled with OpenSSL support.
        $port = isset($uri['port']) ? $uri['port'] : 443;
        $socket = 'ssl://' . $uri['host'] . ':' . $port;
        $options['headers']['Host']
          = $uri['host'] . ($port != 443 ? ':' . $port : '');
        break;

      default:
        watchdog(WATCHDOG_ERROR, 'invalid scheme @scheme', array('scheme' => $uri['scheme']));
        return FALSE;
    }

    $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout']);

    // Make sure the socket opened properly.
    if (!$fp) {
      $error_result = trim($errstr) ? trim($errstr) :
        t('Error opening socket @socket', array('@socket' => $socket));
      watchdog(WATCHDOG_ERROR, $error_result);
      return FALSE;
    }

    // Construct the path to act on.
    $path = isset($uri['path']) ? $uri['path'] : '/';
    if (isset($uri['query'])) {
      $path .= '?' . $uri['query'];
    }

    $request = $options['method'] . ' ' . $path . " HTTP/1.0\r\n";
    foreach ($options['headers'] as $name => $value) {
      $request .= $name . ': ' . trim($value) . "\r\n";
    }
    $request .= "\r\n" . $options['data'];
    $result->request = $request;
    // Calculate how much time is left of the original timeout value.
    $timeout = $options['timeout'] - timer_read(__FUNCTION__) / 1000;
    if ($timeout > 0) {
      stream_set_timeout($fp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
      fwrite($fp, $request);
    }

    // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
    // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
    // instead must invoke stream_get_meta_data() each iteration.
    $info = stream_get_meta_data($fp);
    $alive = !$info['eof'] && !$info['timed_out'];
    $response = '';

    // Read one chunk in an attempt to get the header.
    if ($alive) {
      // Calculate how much time is left of the original timeout value.
      $timeout = $options['timeout'] - timer_read(__FUNCTION__) / 1000;
      if ($timeout <= 0) {
        $info['timed_out'] = TRUE;
      }
      else {
        stream_set_timeout($fp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
        $response = fread($fp, DOCUMENTUM_STREAM_WRAPPER_BUFFER_SIZE);
      }
    }

    if ($info['timed_out']) {
      watchdog(WATCHDOG_ERROR, 'request timed out');
      fclose($fp);
      return FALSE;
    }

    // Parse response headers from the response body.
    // Be tolerant of malformed HTTP responses that separate header and body
    // with \n\n or \r\r instead of \r\n\r\n.
    list($response, $result->data)
      = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
    $response = preg_split("/\r\n|\n|\r/", $response);

    // Parse the response status line.
    $response_status_array
      = _drupal_parse_response_status(trim(array_shift($response)));
    $result->protocol = $response_status_array['http_version'];
    $result->status_message = $response_status_array['reason_phrase'];
    $code = $response_status_array['response_code'];

    $result->headers = array();

    // Parse the response headers.
    while ($line = trim(array_shift($response))) {
      list($name, $value) = explode(':', $line, 2);
      $name = strtolower($name);
      if (isset($result->headers[$name]) && $name == 'set-cookie') {
        // RFC 2109: the Set-Cookie response header comprises the token Set-
        // Cookie:, followed by a comma-separated list of one or more cookies.
        $result->headers[$name] .= ',' . trim($value);
      }
      else {
        $result->headers[$name] = trim($value);
      }
    }

    switch ($code) {
      case 200:
      case 304:
        break;

      default:
        watchdog(WATCHDOG_ERROR, 'Bad response code @code, @message',
          array('code' => $code, 'message' => $result->status_message));
        fclose($fp);
        return FALSE;
    }

    $this->streamFp = $fp;
    $this->streamContent = $result->data;

    return TRUE;
  }

  /**
   * A simple stream read operation.
   *
   * If we've already read data return this pending data, respecting any max
   * data passed from the caller.  If there is no data check that we aren't
   * EOF and then read some more and return it.
   *
   * @param int $count
   *   The maximum amount of data to be fed back to the caller.
   *
   * @return bool|string
   *   The data or FALSE if an error is encountered.
   */
  public function stream_read($count) {
    // If we have no stream content go get some.
    if (!is_string($this->streamContent)) {

      // Fetch response. Due to bugs like http://bugs.php.net/bug.php?id=43782
      // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
      // instead must invoke stream_get_meta_data() each iteration.
      $info = stream_get_meta_data($this->streamFp);
      $alive = !$info['eof'] && !$info['timed_out'];

      $timeout = (float) DOCUMENTUM_STREAM_WRAPPER_READ_TIMEOUT;

      if ($alive) {
        stream_set_timeout($this->streamFp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
        $this->streamContent = fread($this->streamFp, DOCUMENTUM_STREAM_WRAPPER_BUFFER_SIZE);
      }
    }

    // If we have a chunk just return it, respecting the max requested count.
    if (is_string($this->streamContent)) {
      $remaining_chars = strlen($this->streamContent);
      $number_to_read = min($count, $remaining_chars);
      if ($remaining_chars > 0) {
        $buffer = substr($this->streamContent, 0, $number_to_read);
        $this->streamContent = substr($this->streamContent, $number_to_read);
        return $buffer;
      }
    }

    return FALSE;
  }

  /**
   * Check if the stream is done.
   *
   * @return bool
   *   True if EOF has been reached or the stream has timed out,
   *   false otherwise.
   */
  public function stream_eof() {
    // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
    // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
    // instead must invoke stream_get_meta_data() each iteration.
    $info = stream_get_meta_data($this->streamFp);
    $alive = !$info['eof'] && !$info['timed_out'];

    return !$alive;
  }

  /**
   * Support for fclose().
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function stream_close() {
    return fclose($this->streamFp);
  }

  /**
   * Support for fstat().
   *
   * Currently less than ideal as it requires a separate http call for each
   * file to determine filesize.  In a use case of media browsing this could
   * lead to LOTS of http requests.
   *
   * @return array
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  public function stream_stat() {
    $stat = array();

    $headers['Authorization'] = $this->getAuthorizationHeader();
    $headers['Accept'] = 'application/json';

    $options = array(
      'headers' => $headers,
    );

    $content_url = $this->getPropertiesUrl();

    $request = drupal_http_request($content_url, $options);
    if (empty($request->error)) {
      $result_data = json_decode($request->data);
      if (isset($result_data->properties->r_content_size)) {
        $stat['size'] = $result_data->properties->r_content_size;
      }
      elseif ($size = strlen($this->getStreamContent())) {
        // If the properties request does not return a content size info, fall
        // back to performing a full request of the file to determine its file
        // size.
        $stat['size'] = $size;
      }
    }

    return !empty($stat) ? $this->getStat($stat) : FALSE;
  }


  /**
   * Fetch the content of the file using drupal_http_request().
   */
  protected function getStreamContent() {
    if (!isset($this->streamContent)) {
      $this->streamContent = NULL;

      $headers['Authorization'] = $this->getAuthorizationHeader();
      $headers['Accept'] = '*/*';

      $options = array(
        'headers' => $headers,
      );

      $request = drupal_http_request($this->getContentUrl(), $options);
      if (empty($request->error) && !empty($request->data)) {
        $this->streamContent = $request->data;
      }
    }

    return $this->streamContent;
  }

  /**
   * Implementation of getMimeType().
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return DrupalLocalStreamWrapper::getMimeType($uri, $mapping);
  }

}
