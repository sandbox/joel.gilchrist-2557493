CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Documentum Stream Wrapper module provides for the capability to add remote
Documentum content (files) and have Drupal treat them as if they were local
files.  The files are not copied locally but remain in Documentum and are
streamed from Documentum when accessed by a user. The module also includes 
the ability to add files using a file/add tab.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/joel.gilchrist/2557493

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2557493

REQUIREMENTS
------------
This module requires the following modules:
 * File Entity
 * File 
 * Remote Stream Wrapper
 * Encrypt

RECOMMENDED MODULES
-------------------
 * Media

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

   - Access files from documentum

     Users with this permission can access links/files from Documentum 
     for reading.

   - Add files from documentum

     Users with this permission can access the file/add tab for Documentum
     and add new files to the system.

   - Administer the documentum stream wrapper configuration

     Users in roles with this permission can access the configuration menus for
     this module, which allows them to set up multiple backend Documentum 
     repositories.


 * Administer the documentum repository configuration in
   Configuration » Media » Documentum Stream Wrapper


TROUBLESHOOTING
---------------
 * If files cannot be access by readers or added by editors, check the
   following:

   - Drupal logs.  There may be details in the logs which describe the reason
     for failure.

   - Are the Documentum REST services up?  You should be able to hit the url
     in a browser and get content.

   - Is the username and password configuration to access the repository
     correct?


FAQ
---

To be completed.

MAINTAINERS
-----------
Current maintainers:
 * Joel Gilchrist (joel.gilchrist) - https://www.drupal.org/user/2975351
